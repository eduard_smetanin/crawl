// Package disk provides disk write access.
package disk

import (
	"io"
	"os"

	"bitbucket.org/eduard_smetanin/crawl/crawl"
)

// Out implements Out interface that accesses disk.
type Out struct {
}

// Create creates new file, return error if file exists.
func (Out) Create(filename string) (crawl.File, error) {
	return os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0666)
}

// Copy copies file from Reader to Writer.
func (Out) Copy(dst io.Writer, src io.Reader, rawURL string) (written int64, err error) {
	return io.Copy(dst, src)
}

// Remove removes file.
func (Out) Remove(filename string) error {
	return os.Remove(filename)
}
