package logger_test

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"testing"

	"bitbucket.org/eduard_smetanin/crawl/logger"
)

// TestPrintf tests if Printf calls standard library's log.Printf.
func TestPrintf(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	l := logger.Log{}
	s := "運命はない%s"
	suffix := "!"
	l.Printf(s, suffix)
	out := strings.TrimSuffix(fmt.Sprint(buf.String()), "\n")
	expected := fmt.Sprintf(s, suffix)
	if !strings.HasSuffix(out, expected) {
		t.Errorf("log.Printf(%q, %q) logged %q, want suffix %q", s, suffix, out, expected)
	}
}

// TestPrint tests if Print calls standard library's log.Print.
func TestPrint(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	l := logger.Log{}
	s := "運命はない"
	l.Print(s)
	out := strings.TrimSuffix(fmt.Sprint(buf.String()), "\n")
	if !strings.HasSuffix(out, s) {
		t.Errorf("log.Print(%q) logged %q, want suffix %q", s, out, s)
	}
}
