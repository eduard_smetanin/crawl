package logger

import "log"

// Log implements a subset of standard library's package log.
// It just calls standard library's package log methods.
// Instantiate it log := Log{} and then call log.Printf(...) or log.Print(...).
type Log struct {
}

// Printf just calls standard library's log.Printf.
func (Log) Printf(format string, v ...interface{}) {
	log.Printf(format, v...)
}

// Print just calls standard library's log.Print.
func (Log) Print(v ...interface{}) {
	log.Print(v...)
}
