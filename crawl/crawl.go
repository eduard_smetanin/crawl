package crawl

import (
	"io"
)

// File is interface that allows file access on real disk or mock.
type File interface {
	io.Closer
	io.Writer
	Name() string
}

// In is interface to read from sources like web, mock and other.
type In interface {
	// Get returns io.ReadCloser to read file.
	Get(rawURL string) (io.ReadCloser, error)
}

// Out is interface to write files to destinations like disk, mock and other.
type Out interface {
	// Create creates new file, returns error if file exists.
	Create(filename string) (File, error)
	// Copy copies file.
	Copy(dst io.Writer, src io.Reader, rawURL string) (written int64, err error)
	// Remove removes file.
	Remove(filename string) error
}
