// Package web provides web read access.
package web

import (
	"io"
	"net/http"
)

// In implements In interface that reads from web.
type In struct {
}

// Get returns io.ReadCloser to read file.
func (In) Get(rawURL string) (io.ReadCloser, error) {
	resp, err := http.Get(rawURL)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}
