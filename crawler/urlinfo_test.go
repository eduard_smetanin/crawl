package crawler

import (
	"net/url"
	"testing"
)

func TestNewUrlinfo(t *testing.T) {
	verifyNewUrlinfo(
		"",
		"http://foo.com/",
		"http://foo.com",
		"http://foo.com",
		t)
	verifyNewUrlinfo(
		"",
		"http://foo.com",
		"http://foo.com",
		"http://foo.com",
		t)
	verifyNewUrlinfo(
		"http://foo.com",
		"high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://foo.com",
		"/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://foo.com/",
		"high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://foo.com/",
		"/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://foo.com/",
		"high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images/",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://foo.com/",
		"/high-resolution-earth-images",
		"http://foo.com/high-resolution-earth-images",
		"http://foo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://higher-resolution-photo.com/high-resolution-space-images/",
		"/high-resolution-earth-images/",
		"http://higher-resolution-photo.com/high-resolution-earth-images/",
		"http://higher-resolution-photo.com/high-resolution-earth-images",
		t)
	verifyNewUrlinfo(
		"http://higher-resolution-photo.com/high-resolution-space-images/",
		"high-resolution-earth-images/",
		"http://higher-resolution-photo.com/high-resolution-space-images/high-resolution-earth-images/",
		"http://higher-resolution-photo.com/high-resolution-space-images/high-resolution-earth-images",
		t)
}

func verifyNewUrlinfo(baseRawURL, rawURL, expectedRaw, expectedUnified string, t *testing.T) {
	var baseURL *url.URL
	if baseRawURL == "" {
		baseURL = nil
	} else {
		var err error
		baseURL, err = url.Parse(baseRawURL)
		if err != nil {
			t.Errorf("verifyNewUrlinfo: failed to parse base raw URL %q: %v", baseRawURL, err)
		}
	}
	urlinfo, err := newUrlinfo(baseURL, rawURL)
	if err != nil {
		t.Errorf("verifyNewUrlinfo: failed to parse raw URL %q: %v", rawURL, err)
	}
	if urlinfo.Unified != expectedUnified {
		t.Errorf("verifyNewUrlinfo: %q != %q", urlinfo.Unified, expectedUnified)
	}
}
