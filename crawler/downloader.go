package crawler

import (
	"bitbucket.org/eduard_smetanin/crawl/crawl"
	"bitbucket.org/eduard_smetanin/crawl/path"
	"github.com/pkg/errors"
)

// Download checks if file for the URL already exists on disk,
// gets ReadCloser from In, reads from the ReadCloser,
// saves file via Out, reports result through the chan.
func Download(urlinfo urlinfo, in crawl.In, out crawl.Out, doneCh chan<- done, log Log) {
	log.Printf("💾 crawler.Download: open file %q\n", urlinfo.Raw)

	filename, err := path.Filename(urlinfo.Raw)
	if err != nil {
		doneCh <- done{urlinfo: urlinfo, err: errors.Wrapf(err, "crawler.Download: failed extracting filename from %q", urlinfo.Raw)}
		return
	}

	file, err := out.Create(filename)
	if err != nil {
		doneCh <- done{urlinfo: urlinfo, err: errors.Wrapf(err, "crawler.Download: failed opening file %s", filename)}
		return
	}

	log.Printf("⬇ crawler.Download: begin download %q", urlinfo.Raw)

	readCloser, err := in.Get(urlinfo.Raw)
	if err != nil {
		if closeRemoveErr := closeRemove(file, out, log); closeRemoveErr == nil { // If removed successfully.
			log.Printf("💭 crawler.Download: removed file %s after failing to get it", filename)
		}
		doneCh <- done{urlinfo: urlinfo, err: errors.Wrapf(err, "crawler.Download: failed getting file %q", urlinfo.Raw)}
		return
	}
	defer readCloser.Close()

	_, err = out.Copy(file, readCloser, filename)
	if err != nil {
		if closeRemoveErr := closeRemove(file, out, log); closeRemoveErr == nil { // If removed successfully.
			log.Printf("💭 crawler.Download: removed file %s after failing to copy", filename)
		}
		doneCh <- done{urlinfo: urlinfo, err: errors.Wrapf(err, "crawler.Download: failed copying %q", urlinfo.Raw)}
		return
	}

	// Close file manually instead of using defer to return (via chan) possible
	// errors occurred while closing (alternative is to use named return params).
	if err := file.Close(); err != nil {
		doneCh <- done{urlinfo: urlinfo, err: errors.Wrapf(err, "crawler.Download: failed closing file %s after successfull write", filename)}
		return
	}

	doneCh <- done{urlinfo: urlinfo, err: nil}
}

// closeRemove closes and then removes the file, logs errors.
func closeRemove(file crawl.File, out crawl.Out, log Log) error {
	closeErr := file.Close()
	if closeErr != nil {
		log.Printf("❌ crawler.closeRemove: failed closing file %s: %v\n", file.Name(), closeErr) // TODO: Is it good to log and return err?
	}
	removeErr := out.Remove(file.Name())
	if removeErr != nil {
		log.Printf("❌ crawler.closeRemove: failed removing file %s: %v\n", file.Name(), removeErr)
	}
	if closeErr != nil {
		return closeErr
	}
	return removeErr
}
