package crawler_test

import (
	"fmt"
	"io"
	"testing"

	"bitbucket.org/eduard_smetanin/crawl/crawler"
	"bitbucket.org/eduard_smetanin/crawl/logger"
	"bitbucket.org/eduard_smetanin/crawl/mock"
)

type nopReadCloser struct {
	io.Reader
	io.Closer
}

func (*nopReadCloser) Read(p []byte) (n int, err error) { return 0, nil }
func (*nopReadCloser) Close() error                     { return nil }

const (
	high10  = "High_quality_space_photos_10.jpg"
	photo10 = "Photos_space_high_res_file_10.jpg"
	high8   = "High_quality_space_photos_8.jpg"
	high6   = "High_resolution_space_photos_6.jpg"
	photo1  = "Photos_space_high_res_file_1.jpg"
)

var notCoveredCreateRet = mock.CallRet{
	Name: mock.FuncCreate,
	Ret: mock.Ret{
		Foo: (*mock.File)(nil),
		Err: fmt.Errorf("file is not covered by unit testing")}}

// expectedDownloadCalls contains data for testing file download.
var expectedDownloadCalls = map[string][](mock.CallRet){
	high10: {
		mock.CallRet{Name: mock.FuncCreate, Ret: mock.Ret{Foo: &mock.File{Filename: high10}, Err: nil}},
		mock.CallRet{Name: mock.FuncGet, Ret: mock.Ret{Foo: &nopReadCloser{}, Err: nil}},
		mock.CallRet{Name: mock.FuncCopy, Ret: mock.Ret{Foo: int64(0), Err: nil}},
		mock.CallRet{Name: mock.FuncClose, Ret: mock.Ret{Foo: nil, Err: nil}},
	},
	photo10: {
		mock.CallRet{Name: mock.FuncCreate, Ret: mock.Ret{Foo: (*mock.File)(nil), Err: fmt.Errorf("failing Create() for testing purposes")}},
	},
	high8: {
		mock.CallRet{Name: mock.FuncCreate, Ret: mock.Ret{Foo: &mock.File{Filename: high8}, Err: nil}},
		mock.CallRet{Name: mock.FuncGet, Ret: mock.Ret{Foo: (*nopReadCloser)(nil), Err: fmt.Errorf("failing Get() for testing purposes")}},
		mock.CallRet{Name: mock.FuncClose, Ret: mock.Ret{Foo: nil, Err: nil}},
		mock.CallRet{Name: mock.FuncRemove, Ret: mock.Ret{Foo: nil, Err: nil}},
	},
	high6: {
		mock.CallRet{Name: mock.FuncCreate, Ret: mock.Ret{Foo: &mock.File{Filename: high6}, Err: nil}},
		mock.CallRet{Name: mock.FuncGet, Ret: mock.Ret{Foo: &nopReadCloser{}, Err: nil}},
		mock.CallRet{Name: mock.FuncCopy, Ret: mock.Ret{Foo: int64(0), Err: fmt.Errorf("failing Copy() for testing purposes")}},
		mock.CallRet{Name: mock.FuncClose, Ret: mock.Ret{Foo: nil, Err: nil}},
		mock.CallRet{Name: mock.FuncRemove, Ret: mock.Ret{Foo: nil, Err: nil}},
	},
	photo1: {
		mock.CallRet{Name: mock.FuncCreate, Ret: mock.Ret{Foo: &mock.File{Filename: photo1}, Err: nil}},
		mock.CallRet{Name: mock.FuncGet, Ret: mock.Ret{Foo: &nopReadCloser{}, Err: nil}},
		mock.CallRet{Name: mock.FuncCopy, Ret: mock.Ret{Foo: int64(0), Err: nil}},
		mock.CallRet{Name: mock.FuncClose, Ret: mock.Ret{Foo: nil, Err: fmt.Errorf("failing Close() for testing purposes")}},
	},
	"Photos_space_high_res_file_2.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_3.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_4.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_5.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_6.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_7.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_8.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_9.jpg":   {notCoveredCreateRet},
	"Photos_space_high_res_file_11.jpg":  {notCoveredCreateRet},
	"High_resolution_space_photos_1.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_2.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_3.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_4.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_5.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_7.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_8.jpg": {notCoveredCreateRet},
	"High_resolution_space_photos_9.jpg": {notCoveredCreateRet},
	"High_quality_space_photos_1.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_2.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_3.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_4.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_5.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_6.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_7.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_9.jpg":    {notCoveredCreateRet},
	"High_quality_space_photos_11.jpg":   {notCoveredCreateRet},
}

// expectedPages contains data for testing URLs found on website.
// Data is stored in map (not array) because order of downloads differs because of how select statement works.
var expectedPages = map[string]struct{}{
	"http://higher-resolution-photo.com/high-resolution-space-images/":            struct{}{},
	"http://higher-resolution-photo.com/":                                         struct{}{},
	"http://higher-resolution-photo.com/category/space/":                          struct{}{},
	"http://higher-resolution-photo.com/category/space-images/":                   struct{}{},
	"http://higher-resolution-photo.com/waxing-crescent-moon/":                    struct{}{},
	"http://higher-resolution-photo.com/hubble-telescope-images-high-resolution/": struct{}{},
	"http://higher-resolution-photo.com/hubble-images-high-resolution/":           struct{}{},
	"http://higher-resolution-photo.com/solar-system-order/":                      struct{}{},
	"http://higher-resolution-photo.com/category/earth/":                          struct{}{},
	"http://higher-resolution-photo.com/category/hubble-telescope/":               struct{}{},
	"http://higher-resolution-photo.com/category/moon-photos/":                    struct{}{},
	"http://higher-resolution-photo.com/category/solar-system/":                   struct{}{},
	"http://higher-resolution-photo.com/cookie-policy/":                           struct{}{},
	"http://higher-resolution-photo.com/disclaimer/":                              struct{}{},
	"http://higher-resolution-photo.com/privacy-policy/":                          struct{}{},
	"http://higher-resolution-photo.com/terms-and-conditions/":                    struct{}{},
	"http://higher-resolution-photo.com/high-resolution-earth-images/":            struct{}{},
}

func TestCrawlWebsite(t *testing.T) {
	mock.ExpectedDownloadCalls = expectedDownloadCalls
	mock.ExpectedPages = expectedPages
	website := "http://higher-resolution-photo.com/high-resolution-space-images/"
	in := mock.In{
		PersistedURLs: map[string]struct{}{
			"high-resolution-space-images": struct{}{},
		},
		T: t,
	}
	out := mock.Out{T: t}
	log := logger.Log{}
	exitCh := make(chan interface{})
	err := crawler.Run(website, in, out, exitCh, log)
	if err != nil {
		t.Errorf("TestCrawlWebsite: failed to crawl website %q: %v", website, err)
		return
	}
	mock.VerifyCrawledPages(t)
	mock.VerifyDownloadCalls(t)
}
