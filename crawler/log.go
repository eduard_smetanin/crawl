package crawler

// Log contains a subset of standard library's package log needed by crawler.
// It's needed because crawler's package user may not want
// crawler to log or may want to log into a different destination.
type Log interface {
	// Print signature is the same as standard library's log.Print.
	Print(v ...interface{})
	// Printf signature is the same as standard library's log.Printf.
	Printf(format string, v ...interface{})
}
