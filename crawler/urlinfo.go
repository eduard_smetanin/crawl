package crawler

import (
	"net/url"
	"strings"
)

// urlinfo contains raw, unified (standardized) and parsed versions of a URL.
type urlinfo struct {
	// Raw is URL string provided by user or found in html as is,
	// except for relative once that get converted to absolute URLs.
	Raw string
	// Unified is reconstructed URL string from parsed URL without
	// terminal slash; can be used as URL ID.
	Unified string
	// Parsed is parsed URL struct.
	Parsed *url.URL
}

// newUrlinfo creates new urlinfo by parsing the raw URL,
// converting relative URL to absolute URL and removing trailing slash.
func newUrlinfo(baseURL *url.URL, rawURL string) (urlinfo, error) {
	u, err := url.Parse(rawURL)
	if err != nil {
		return urlinfo{}, err
	}
	absURL := baseURL.ResolveReference(u)
	absRawURL := absURL.String()
	unified := strings.TrimSuffix(absRawURL, "/")
	return urlinfo{Raw: absRawURL, Unified: unified, Parsed: absURL}, nil
}

func isRelativeURL(url *url.URL) bool {
	return len(url.Hostname()) == 0
}

func concat(baseRawURL, rawURL string) string {
	return strings.TrimSuffix(baseRawURL, "/") + "/" + strings.TrimPrefix(rawURL, "/")
}
