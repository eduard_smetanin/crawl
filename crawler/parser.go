package crawler

import (
	"io"
	"strings"

	"bitbucket.org/eduard_smetanin/crawl/crawl"
	"bitbucket.org/eduard_smetanin/crawl/path"
	"github.com/pkg/errors"
	"golang.org/x/net/html"
)

// Parse gets ReadCloser from In, reads from the ReadCloser,
// parses the web page and returns found page and file URLs through the chans.
func Parse(pageURLInfo urlinfo, in crawl.In, chans parseOutChans, counter adder, log Log) {
	readCloser, err := in.Get(pageURLInfo.Raw)
	if err != nil {
		chans.done <- done{urlinfo: pageURLInfo, err: errors.Wrapf(err, "crawl: failed getting %q", pageURLInfo.Raw)}
		return
	}
	defer readCloser.Close()
	tokenizer := html.NewTokenizer(readCloser)
	for {
		tokenType := tokenizer.Next()
		switch tokenType {
		case html.ErrorToken:
			tokenizerErr := tokenizer.Err()
			if tokenizerErr != io.EOF {
				chans.done <- done{urlinfo: pageURLInfo, err: errors.Wrapf(tokenizerErr, "✖ crawl: failed getting token in %q", pageURLInfo.Raw)}
				return
			}
			chans.done <- done{urlinfo: pageURLInfo, err: nil}
			return
		case html.StartTagToken:
			token := tokenizer.Token()
			href, ok := href(token)
			if !ok {
				continue
			}
			linkURLInfo, err := newUrlinfo(pageURLInfo.Parsed, href)
			if err != nil {
				log.Printf("✖ crawler.Parse: failed parsing URL %q in %q: %v", href, pageURLInfo.Raw, err)
				continue
			}
			// TODO: Read link and determine it's type instead of guessing by URL?
			if path.IsDownloadableFile(linkURLInfo.Parsed) {
				counter.Add(1)
				chans.file <- linkURLInfo
				continue
			}
			if !path.IsPage(linkURLInfo.Parsed) {
				log.Printf("⏩ crawler.Parse: skipping URL %q in %q", href, pageURLInfo.Raw)
				continue
			}
			if linkURLInfo.Parsed.Hostname() == pageURLInfo.Parsed.Hostname() {
				counter.Add(1)
				chans.page <- linkURLInfo
				continue
			}
		}
	}
}

// href returns href from <a> html tag, if any.
func href(token html.Token) (href string, ok bool) {
	if !strings.EqualFold(token.Data, "a") {
		return "", false
	}
	href, ok = attributeVal(token, "href")
	if !ok {
		return "", false
	}
	if strings.EqualFold(href, "#top") || strings.EqualFold(href, "#bot") {
		return "", false
	}
	rel, ok := attributeVal(token, "rel")
	if ok && strings.EqualFold(rel, "nofollow") {
		return "", false
	}
	return href, true
}

// attributeVal pulls attribute value by attribute key from the token.
func attributeVal(token html.Token, attributeKey string) (string, bool) {
	for _, attribute := range token.Attr {
		if strings.EqualFold(attribute.Key, attributeKey) {
			return attribute.Val, true
		}
	}
	return "", false
}
