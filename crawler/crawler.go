// Package crawler crawls website and downloads files.
// Input and output performed by injected instances of types
// that implement interfaces In and Out correspondingly.
package crawler

import (
	"fmt"
	"net/url"
	"os"
	"sync"

	"bitbucket.org/eduard_smetanin/crawl/crawl"
	"bitbucket.org/eduard_smetanin/crawl/path"
	"github.com/pkg/errors"
)

// done contains urlinfo for crawled webpage/downloaded file or error.
type done struct {
	urlinfo urlinfo
	err     error
}

// parseOutChans contains chans to receive info over from a crawling function.
type parseOutChans struct {
	page chan<- urlinfo
	file chan<- urlinfo
	done chan<- done
}

type doner interface {
	Done()
}

type adder interface {
	Add(int)
}

// Run reads content of the website, parses found webpages, downloads found files, logs activity.
func Run(website string, in crawl.In, out crawl.Out, userExitCh chan interface{}, log Log) error {
	siteURLInfo, err := newUrlinfo(&url.URL{}, website)
	if err != nil {
		return err
	}
	if len(siteURLInfo.Parsed.Hostname()) == 0 {
		return fmt.Errorf("crawler.Run: failed getting host name from %q", website)
	}
	doneCh := make(chan done, 10000000)
	fileFoundCh := make(chan urlinfo, 10000000) // TODO: Consider alternatives to hardcoding a large size.
	pageFoundCh := make(chan urlinfo, 10000000)
	fileQueueCh := make(chan urlinfo, 10000000)
	pageQueueCh := make(chan urlinfo, 10000000)
	var counter sync.WaitGroup
	failExitCh := make(chan interface{}) // failExitCh needs to be closed to signal exit.
	exitCh := fanInExit(userExitCh, failExitCh, log)
	resultCh := successCounter(failExitCh, log) // Success/failure counter to know when to exit because of too many errors.
	processResults(doneCh, &counter, resultCh, pageQueueCh, fileQueueCh, log)
	filterFiles(fileQueueCh, fileFoundCh, &counter, log)
	filterPages(siteURLInfo, pageQueueCh, pageFoundCh, &counter, log)
	counter.Add(1)
	go scheduleDownloads(doneCh, fileQueueCh, in, out, exitCh, log)
	outChans := parseOutChans{page: pageFoundCh, file: fileFoundCh, done: doneCh}
	go scheduleParsing(outChans, pageQueueCh, in, &counter, exitCh, log)
	counter.Wait()
	return nil
}

func processResults(doneCh <-chan done, counter doner, resultCh chan<- bool, pageQueueCh, fileQueueCh chan<- urlinfo, log Log) {
	go func() {
		for {
			select {
			case result := <-doneCh:
				processResult(result, counter, resultCh, pageQueueCh, fileQueueCh, log)
			}
		}
	}()
}

func fanInExit(userExitCh, failExitCh <-chan interface{}, log Log) chan interface{} {
	exitCh := make(chan interface{})
	go func() {
		for {
			select {
			case <-userExitCh:
				close(exitCh)
				bye(log)
				return
			case <-failExitCh:
				close(exitCh)
				bye(log)
				return
			default:
			}
		}
	}()
	return exitCh
}

func bye(log Log) {
	log.Print("👋 Exiting...")
}

// scheduleParsing launches several goroutines to parse webpages.
func scheduleParsing(outChans parseOutChans, pageQueueCh chan urlinfo, in crawl.In, counter adder, exitCh <-chan interface{}, log Log) {
	pageSemaphoreCh := make(chan struct{}, 5)
	for {
		pageSemaphoreCh <- struct{}{} // Write to get a slot to run goroutine.
		go func() {
			defer func() {
				<-pageSemaphoreCh // Read to release a slot to run goroutine.
			}()
			select {
			case urlinfo := <-pageQueueCh:
				select {
				case <-exitCh:
					outChans.done <- done{urlinfo: urlinfo, err: errors.Errorf("scheduleParsing: exiting %q", urlinfo.Raw)}
					return
				default:
					Parse(urlinfo, in, outChans, counter, log)
				}
			default:
			}
		}()
	}
}

// scheduleDownloads launches several goroutines to download files.
func scheduleDownloads(doneCh chan done, fileQueueCh chan urlinfo, in crawl.In, out crawl.Out, exitCh <-chan interface{}, log Log) {
	fileSemaphoreCh := make(chan struct{}, 5)
	for {
		fileSemaphoreCh <- struct{}{} // Write to get a slot to run goroutine.
		go func() {
			defer func() {
				<-fileSemaphoreCh // Read to release a slot to run goroutine.
			}()
			select {
			case urlinfo := <-fileQueueCh:
				select {
				case <-exitCh:
					doneCh <- done{urlinfo: urlinfo, err: errors.Errorf("scheduleDownloads: exiting %q", urlinfo.Raw)}
					return
				default:
					Download(urlinfo, in, out, doneCh, log)
				}
			default:
			}
		}()
	}
}

// filterPages listens on pageFoundCh, checks if the page has already been processed,
// if it's new - marks it as processed and sends it to pageQueueCh, if it's been processed - ignores it.
func filterPages(website urlinfo, pageQueueCh chan urlinfo, pageFoundCh chan urlinfo, counter doner, log Log) {
	processed := make(map[string]struct{})
	// Adding website to processed right away, not in goroutine, so sync.WaitGroup.Wait() in Run() can work.
	processed[website.Unified] = struct{}{} // Mark page as processed to avoid double processing in the future.
	pageQueueCh <- website
	log.Printf("🕗🌏 crawler.filterPages: queue %q\n", website.Raw)
	go func() {
		for urlinfo := range pageFoundCh {
			before := len(processed)
			processed[urlinfo.Unified] = struct{}{} // Mark page as processed to avoid double processing in the future.
			after := len(processed)
			if before == after {
				counter.Done()
				continue // Ignore page because it's been processed before.
			}
			pageQueueCh <- urlinfo
			log.Printf("🕗🌏 crawler.filterPages: queue %q\n", urlinfo.Raw)
		}
	}()
}

// filterFiles listens on fileFoundCh, checks if the file has already been processed,
// if it's new - marks it as processed and sends it to fileQueueCh, if it's been processed - ignores it.
func filterFiles(fileQueueCh chan urlinfo, fileFoundCh chan urlinfo, counter doner, log Log) {
	processed := make(map[string]struct{})
	go func() {
		for urlinfo := range fileFoundCh {
			before := len(processed)
			processed[urlinfo.Unified] = struct{}{} // Mark file as processed to avoid double processing in the future.
			after := len(processed)
			if before == after {
				counter.Done()
				continue // Ignore file because it's been processed before.
			}
			fileQueueCh <- urlinfo
			log.Printf("🕗🐠 crawler.filterFiles: queue %q\n", urlinfo.Raw)
		}
	}()
}

// processResult keeps track successful and failed crawls/downloads, retries if appropriate.
func processResult(result done, counter doner, resultCh chan<- bool, pageQueueCh, fileQueueCh chan<- urlinfo, log Log) {
	isDownloadableFile := path.IsDownloadableFile(result.urlinfo.Parsed)
	if result.err != nil {
		cause := errors.Cause(result.err)
		if os.IsExist(cause) {
			// File exists is not a problem, it just means the file has been downloaded before.
			log.Printf("✅ crawler.processResult: %v", result.err)
			counter.Done()
			return
		}
		resultCh <- false
		if cause == nil {
			log.Printf("❌ crawler.processResult: %v", result.err)
			counter.Done()
			return
		}
		// Note, Temporary() is not perfect because depenging on the way
		// site rejects us we may or may not consider the error temporary.
		temp, ok := cause.(interface{ Temporary() bool })
		canRetry := ok && temp.Temporary()
		log.Printf("❌ crawler.processResult: temporaryOk=%t, canRetry=%t, cause type=%T: %v", ok, canRetry, cause, result.err)
		if !canRetry {
			counter.Done()
			return
		}
		if isDownloadableFile {
			fileQueueCh <- result.urlinfo
		} else {
			pageQueueCh <- result.urlinfo
		}
		return // Not calling counter.Done() before return because of retry.
	}
	resultCh <- true
	if isDownloadableFile {
		log.Printf("️️️️❤️  crawler.processResult: downloaded %q", result.urlinfo.Raw)
	} else {
		log.Printf("✔ crawler.processResult: crawled %q", result.urlinfo.Raw)
	}
	counter.Done()
}

// successCounter returns chan that it listens to on goroutine to count successes/failures.
// If there are too many failures it closes failExitCh to indicate program needs to exit.
func successCounter(failExitCh chan<- interface{}, log Log) chan<- bool {
	resultCh := make(chan bool, 10000000)
	go func() {
		successCount := 0
		failCount := 0
		for success := range resultCh {
			if success {
				successCount++
			} else {
				failCount++
				log.Printf("∑ crawler.successCounter: error count: %d\n", failCount)
			}
			if float64(failCount) > max(100, 0.1*float64(successCount)) {
				log.Printf("🏁❌ crawler.successCounter: too many failures (%d)\n", failCount)
				close(failExitCh)
				return
			}
		}
	}()
	return resultCh
}

// max is implemented here to avoid dependency on math package.
// Logic is simplified.
func max(x, y float64) float64 {
	if x > y {
		return x
	}
	return y
}
