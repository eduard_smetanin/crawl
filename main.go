package main

import (
	"fmt"
	"os"
	"os/signal"

	"bitbucket.org/eduard_smetanin/crawl/crawler"
	"bitbucket.org/eduard_smetanin/crawl/disk"
	"bitbucket.org/eduard_smetanin/crawl/logger"
	"bitbucket.org/eduard_smetanin/crawl/web"
)

func main() {
	args := os.Args[1:]
	argCount := len(args)
	if argCount == 0 {
		fmt.Printf("Crawl is a tool to bulk download images and zip archives from a website.\n\nUsage:\n\n\tcrawl websiteURL\n\n")
		return
	}
	website := args[argCount-1]
	userExitCh := make(chan interface{})
	interruptCh := make(chan os.Signal, 1)
	signal.Notify(interruptCh, os.Interrupt) // Sign up to receive notification from os when user presses Ctrl+C.
	finishCh := make(chan interface{})
	go func() {
		err := crawler.Run(website, web.In{}, disk.Out{}, userExitCh, logger.Log{})
		if err != nil {
			fmt.Printf("❌ main: failed crawling website %q: %v\n", website, err)
		}
		finishCh <- struct{}{}
	}()
	for {
		select {
		case <-interruptCh:
			userExitCh <- struct{}{}
			fmt.Println("❌ main: interrupt signal received, waiting for download to exit")
		case <-finishCh:
			fmt.Println("🏁 main: done")
			return
		}
	}
}
