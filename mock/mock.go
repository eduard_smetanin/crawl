// Package mock provides mock implementations of interfaces.
package mock

import (
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"testing"

	"bitbucket.org/eduard_smetanin/crawl/crawl"
	"bitbucket.org/eduard_smetanin/crawl/path"
)

// crawlPageCh is used to report webpages being read.
var crawlPageCh = make(chan string, 10000)

// downloadCallCh is used to report func names called during file download.
var downloadCallCh = make(chan filenameCall, 10000)

const (
	// FuncCreate is name of function Create.
	FuncCreate = "Create"
	// FuncGet is name of function Get.
	FuncGet = "Get"
	// FuncCopy is name of function Copy.
	FuncCopy = "Copy"
	// FuncClose is name of function Close.
	FuncClose = "Close"
	// FuncRemove is name of function Remove.
	FuncRemove = "Remove"
)

// File is a mock implementation of crawl.File interface.
type File struct {
	Filename string
}

// Read mocks reading from file.
func (f *File) Read(p []byte) (n int, err error) {
	return 0, nil
}

// Close mocks closing file.
func (f *File) Close() error {
	downloadCallCh <- filenameCall{f.Filename, FuncClose}
	return nil
}

// Write mocks writing to file.
func (f *File) Write(p []byte) (n int, err error) {
	return 0, nil
}

// Name mocks filename.
func (f *File) Name() string {
	return f.Filename
}

// In is a mock implementation of In interface.
type In struct {
	// PersistedURLs contains filenames of html files included into the test project.
	PersistedURLs map[string]struct{}
	T             *testing.T
}

// Get returns ReadCloser to read from the URL.
func (in In) Get(rawURL string) (io.ReadCloser, error) {
	u, err := url.Parse(rawURL)
	if err != nil {
		return nil, fmt.Errorf("mock.Get: failed to parse URL %q: %v", rawURL, err)
	}
	filename, err := path.Filename(rawURL)
	if err != nil {
		return nil, err
	}
	if path.IsDownloadableFile(u) {
		downloadCallCh <- filenameCall{filename, FuncGet}
		ret := retVal(filename, FuncGet, in.T)
		return ret.Foo.(io.ReadCloser), ret.Err
	}
	if !path.IsPage(u) {
		return nil, fmt.Errorf("mock.Get: URL %q is not file nor page", rawURL)
	}
	crawlPageCh <- rawURL
	if !in.Persisted(filename) {
		// Not all webpages are persisted for unit testing;
		// returning an error will skip web page.
		return nil, fmt.Errorf("mock.Get: URL %q is not covered by unit testing", rawURL)
	}
	f, err := os.Open(filename) // Some webpages are saved on disk in the project directory for testing.
	return f, err
}

// Persisted indicates if the file exists in the project directory.
func (in In) Persisted(name string) bool {
	_, ok := in.PersistedURLs[name]
	return ok
}

// Out is a mock implementation of Out interface.
type Out struct {
	T *testing.T
}

// Create creates new file. Returns error if file exists.
func (ou Out) Create(filename string) (crawl.File, error) {
	downloadCallCh <- filenameCall{filename, FuncCreate}
	ret := retVal(filename, FuncCreate, ou.T)
	return ret.Foo.(crawl.File), ret.Err
}

// Copy copies file.
func (ou Out) Copy(dst io.Writer, src io.Reader, filename string) (written int64, err error) {
	downloadCallCh <- filenameCall{filename, FuncCopy}
	ret := retVal(filename, FuncCopy, ou.T)
	return ret.Foo.(int64), ret.Err
}

// Remove removes file.
func (ou Out) Remove(filename string) error {
	downloadCallCh <- filenameCall{filename, FuncRemove}
	ret := retVal(filename, FuncRemove, ou.T)
	return ret.Err
}

// Ret contains value pair for mock function return.
type Ret struct {
	Foo interface{}
	Err error
}

// CallRet contains mock function name/return value pair.
type CallRet struct {
	Name string
	Ret  Ret
}

// filenameCall contains a filename/funcname to report sequence of func calls.
type filenameCall struct {
	filename string
	funcName string
}

// ExpectedDownloadCalls contains map of func names with return values.
var ExpectedDownloadCalls map[string][](CallRet)

// ExpectedPages contains URLs of webpages that are expected to be found in test crawl.
// Map (not array) is used because order of downloads differs because of how select statement works.
var ExpectedPages map[string]struct{}

// retVal returns return value to be returned from mock functions.
func retVal(filename, funcName string, t *testing.T) Ret {
	calls := ExpectedDownloadCalls[filename]
	for i := range calls {
		call := calls[i]
		if call.Name == funcName {
			return call.Ret
		}
	}
	t.Fatalf("mock.retVal: unexpected function %s for URL %q", funcName, filename)
	return Ret{}
}

// actualDownloadCalls reads all data from DownloadCallCh and returns map of func call names for every URL.
func actualDownloadCalls() map[string][]string {
	close(downloadCallCh)
	callMap := make(map[string][]string)
	for madeCall := range downloadCallCh {
		recordedCalls := callMap[madeCall.filename]
		if recordedCalls == nil {
			callMap[madeCall.filename] = []string{madeCall.funcName}
			continue
		}
		callMap[madeCall.filename] = append(recordedCalls, madeCall.funcName)
	}
	return callMap
}

// equal compares two arrays of strings.
func equal(lhs, rhs []string) bool {
	if len(lhs) != len(rhs) {
		return false
	}
	for i, v := range lhs {
		if v != rhs[i] {
			return false
		}
	}
	return true
}

// names makes array of func names from array of CallRet.
func names(c []CallRet) []string {
	var names []string
	for _, calls := range c {
		names = append(names, calls.Name)
	}
	return names
}

// VerifyCrawledPages checks if set of actually crawled webpages is as expected.
func VerifyCrawledPages(t *testing.T) {
	close(crawlPageCh)
	for page := range crawlPageCh {
		countBefore := len(ExpectedPages)
		delete(ExpectedPages, page)
		countAfter := len(ExpectedPages)
		if countBefore == countAfter {
			t.Errorf("mock.VerifyCrawledPages: unexpected page %q", page)
		}
	}
	if len(ExpectedPages) != 0 {
		var strs []string
		for k := range ExpectedPages {
			strs = append(strs, k)
		}
		t.Errorf("mock.VerifyCrawledPages: %d pages not crawled:\n%s", len(ExpectedPages), strings.Join(strs, "\n"))
	}
}

// VerifyDownloadCalls checks if func calls during files download were made as expected.
func VerifyDownloadCalls(t *testing.T) {
	actualCalls := actualDownloadCalls()
	for filename, expectedCalls := range ExpectedDownloadCalls {
		actualCallNames, ok := actualCalls[filename]
		delete(actualCalls, filename)
		if !ok {
			t.Errorf("mock.VerifyDownloadCalls: no calls made for %s", filename)
			continue
		}
		expectedCallNames := names(expectedCalls)
		if !equal(expectedCallNames, actualCallNames) {
			t.Errorf("mock.VerifyDownloadCalls: calls don't match: expected %v, actual %v for %s",
				expectedCallNames, actualCallNames, filename)
		}
	}
	if len(actualCalls) != 0 {
		t.Errorf("mock.VerifyDownloadCalls: %d unexpected pages:\n%v", len(actualCalls), actualCalls)
	}
}
