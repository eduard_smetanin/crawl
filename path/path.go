// Package path provides functions working with paths (URLs).
package path

import (
	"fmt"
	"net/url"
	"path"
	"strings"
)

// Filename extracts filename, if any, from the URL string.
func Filename(rawURL string) (string, error) {
	name := path.Base(rawURL)
	if name == "/" || name == "." {
		return "", fmt.Errorf("path.Filename: no filename found in URL %q", rawURL)
	}
	return name, nil
}

// IsDownloadableFile indicates if URL represents a downloadable file.
func IsDownloadableFile(u *url.URL) bool {
	if u.Scheme != "http" && u.Scheme != "https" {
		return false
	}
	filename, err := Filename(u.Path)
	if err != nil {
		return false
	}
	ext := path.Ext(u.Path)
	filenameLen := len(filename) - len(ext)
	return filenameLen > 0 && (ext == ".png" || ext == ".jpg" || ext == ".jpeg" || ext == ".zip")
}

// IsPage indicates if URL represents a link to a webpage.
func IsPage(u *url.URL) bool {
	if u.Host == "" && u.Path == "" {
		return false
	}
	if !strings.Contains(u.Path, "/") {
		return true
	}
	ext := path.Ext(u.Path)
	if ext == ".htm" || ext == ".html" || ext == ".xhtml" || ext == ".jhtml" {
		return true
	}
	return len(ext) == 0
}
