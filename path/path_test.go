package path_test

import (
	"fmt"
	"net/url"
	"testing"

	"bitbucket.org/eduard_smetanin/crawl/path"
)

var isDownloadableFileTests = []struct {
	in  string
	out bool
}{
	{"http://abc.com/123.png", true},
	{"http://abc.com/123.jpeg", true},
	{"http://abc.com/こんにちは.png", true},
	{"http://abc.com/привет.jpg", true},
	{"http://abc.com/🔵.jpeg", true},
	{"http://abc.com/123.img", false},
	{"http://abc.com/привет.123", false},
	{"http://abc.com/png.こんにちは", false},
	{"http://abc.com/🔵.🔵", false},
	{"http://abc.com/🔵.🔵🔵", false},
	{"http://abc.com/.🔵", false},
	{"http://abc.com/🔵.", false},
	{"http://abc.com/🔵🔵.", false},
	{"http://abc.com/abc.", false},
	{"http://abc.com/.", false},
	{"http://abc.com/..", false},
	{"http://abc.com/.jpg", false},
	{"http://abc.com//", false},
	{"http://abc.com///", false},
	{"http://abc.com////", false},
	{"http://higher-resolution-photo.com/", false},
	{"www.abc.com/123.jpg", false},    // TODO: Should this be true? (convert www to http?)
	{"www.site.com/こんにちは.png", false}, // TODO: Should this be true? (convert www to http?)
	{"http://www.worldwidetelescope.org/wwtweb/ShowImage.aspx?thumb=http%3A%2F%2Fwww.spacetelescope.org%2Fstatic%2Farchives%2Fimages%2Fthumbs%2Fheic0604a.jpg", false},
	{"mailto: hubble@eso.org", false},
}

func TestIsDownloadableFile(t *testing.T) {
	for _, tt := range isDownloadableFileTests {
		u, err := url.Parse(tt.in)
		if err != nil {
			t.Errorf("TestIsDownloadableFile: failed to parse URL %q", tt.in)
		}
		b := path.IsDownloadableFile(u)
		if b != tt.out {
			t.Errorf("path.IsDownloadableFile(%q) => %t, want %t", tt.in, b, tt.out)
		}
	}
}

const errMsg = "path.Filename: no filename found in URL %q"

var filenameTests = []struct {
	in  string
	out string
	err error
}{
	{"123.png", "123.png", nil},
	{"www.site.com/abc.png", "abc.png", nil},
	{"https://www.foo.com/qwe.jpg", "qwe.jpg", nil},
	{"https://foo.com/こんにちは.jpeg", "こんにちは.jpeg", nil},
	{"https://foo.com/привет", "привет", nil},
	{"https://foo.com/", "foo.com", nil},
	{"https://foo.com", "foo.com", nil},
	{"", "", fmt.Errorf(errMsg, "")},
	{".", "", fmt.Errorf(errMsg, ".")},
}

func TestFilename(t *testing.T) {
	for _, tt := range filenameTests {
		name, err := path.Filename(tt.in)
		if name != tt.out || !equal(err, tt.err) {
			t.Errorf("path.Filename(%q) => (%q, %v), want (%q, %v)", tt.in, name, err, tt.out, tt.err)
		}
	}
}

func equal(lhv, rhv error) bool {
	if lhv == nil && rhv == nil {
		return true
	}
	if lhv == nil || rhv == nil {
		return false
	}
	return lhv.Error() == rhv.Error()
}

var isPageTests = []struct {
	in  string
	out bool
}{
	{"http://higher-resolution-photo.com", true},
	{"http://higher-resolution-photo.com/", true},
	{"site.com", true},
	{"www.site.com/abc", true},
	{"www.site.com/", true},
	{"www.site.com", true},
	{"https://foo.com/page.htm", true},
	{"https://foo.com/page.html", true},
	{"https://foo.com/page.xhtml", true},
	{"https://foo.com/page.jhtml", true},
	{"https://foo.com/", true},
	{"https://foo.com", true},
	{"https://foo.com/abc", true},
	{"https://foo.com/привет", true},
	{"www.site.com/abc.org/def", true},
	{"www.site.com/abc.def", false},
	{"www.site.com/abc.com", false},
	{"www.site.com/abc.png", false},
	{"https://www.foo.com/qwe.jpg", false},
	{"https://foo.com/こんにちは.jpeg", false},
	{"", false},
}

func TestIsPage(t *testing.T) {
	for _, tt := range isPageTests {
		u, err := url.Parse(tt.in)
		if err != nil {
			t.Errorf("TestIsPage: failed to parse URL %q", tt.in)
		}
		isPage := path.IsPage(u)
		if isPage != tt.out {
			t.Errorf("path.IsPage(%q) => %t, want %t", tt.in, isPage, tt.out)
		}
	}
}
